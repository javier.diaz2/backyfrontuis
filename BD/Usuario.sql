DROP DATABASE IF EXISTS usuarios;

CREATE DATABASE usuarios;

USE usuarios;

CREATE TABLE tipoUsuario{
idTipo INT(10) NOT NULL,
descripcion VARCHAR(10) NOT NULL,
PRIMARY KEY(idTipo));

INSERT INTO tipoUsuario VALUES (1,"Administrador"),(2,"Vendedor");


CREATE TABLE usuario(
id INT(10) AUTO_INCREMENT,
nombre VARCHAR(30) NOT NULL,
ciudad VARCHAR(30) NOT NULL,
telefono VARCHAR(15) NOT NULL,
idTipo INT(1) NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY(idTipo) REFERENCES tipoUsuario(idTipo));

INSERT INTO usuario  (nombre,ciudad,telefono) 
VALUES("jorge","bogota","315964856",1),("Fran","Cali","314686856",1),("Hector","Medellin","311986856",2);

SELECT * FROM usuario;