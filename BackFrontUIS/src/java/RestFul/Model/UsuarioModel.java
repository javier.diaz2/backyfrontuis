
package RestFul.Model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UsuarioModel {
    private int id;
    private String nombre;
    private String ciudad;
    private String telefono;
    private int idTipo;

    public UsuarioModel() {
    }

    public UsuarioModel(int id, String nombre, String ciudad, String telefono, int idTipo) {
        this.id = id;
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.idTipo = idTipo;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    
}
