
package RestFul.Model;

import java.sql.Connection;
import java.sql.DriverManager;


public class Conexion {
    private Connection con;
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String HOST = "localhost:3306";
    private String DB = "usuarios";
    private String URL = "jdbc:mysql://" + HOST + "/" + DB + "?serverTimezone=UTC";
    private String USERNAME = "root";
    private String PASSWORD = "";

    public Conexion() {

        try {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            con.setTransactionIsolation(8);
            System.out.println("Conexión exitosa");
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }
    
    public Connection getCon() {
        return con;
    }

    public void desconectar() {
        try {
            if (con != null) {
                con.close();
                System.out.println("La desconexion fue exitosa");
            }

        } catch (Exception excepcion) {
            System.out.println("Ha ocurrido un error al desconectar  " + excepcion.getMessage());
        }
    }
}
