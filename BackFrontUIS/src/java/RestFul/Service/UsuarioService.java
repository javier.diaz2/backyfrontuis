package RestFul.Service;

import RestFul.Model.Conexion;
import RestFul.Model.UsuarioModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class UsuarioService {

    public ArrayList<UsuarioModel> getUsuarios() {
        ArrayList<UsuarioModel> lista = new ArrayList<>();
        Conexion con = new Conexion();
        String sql = "SELECT * FROM usuario";

        try {
            Statement stm = con.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                UsuarioModel usuario = new UsuarioModel();
                usuario.setId(rs.getInt("id"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setCiudad(rs.getString("ciudad"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setIdTipo(rs.getInt("idTipo"));
                lista.add(usuario);
            }
        } catch (Exception e) {

            return lista;
        }

        return lista;
    }

    public UsuarioModel getUsuario(int id) {
        UsuarioModel usuario = new UsuarioModel();
        Conexion con = new Conexion();
        String sql = "SELECT * FROM usuario WHERE id = ?";

        try {
            PreparedStatement pstm = con.getCon().prepareStatement(sql);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                usuario.setId(rs.getInt("id"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setCiudad(rs.getString("ciudad"));
                usuario.setTelefono(rs.getString("telefono"));

            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return usuario;
        }

        return usuario;
    }

    public String delUsuario(int id) {

        Conexion con = new Conexion();
        String sql = "DELETE FROM usuario WHERE id = ?";

        try {
            PreparedStatement pstm = con.getCon().prepareStatement(sql);
            pstm.setInt(1, id);
            return "{\"Accion\":\"" + pstm.executeUpdate() + "\"}";
        } catch (Exception e) {

            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"error\"}";
        }

    }

    public String setUsuario(UsuarioModel usuario) {
        Conexion con = new Conexion();
        String sql = "INSERT INTO usuario  (nombre,ciudad,telefono,idTipo) VALUES (?,?,?,?)";
        try {
            PreparedStatement pstm = con.getCon().prepareStatement(sql);
            pstm.setString(1, usuario.getNombre());
            pstm.setString(2, usuario.getCiudad());
            pstm.setString(3, usuario.getTelefono());
            pstm.setInt(4, usuario.getIdTipo());
            System.out.println("pstm = " + pstm);
            return "{\"Accion\":\"" + pstm.executeUpdate() + "\"}";
        } catch (Exception e) {
            UsuarioModel usu = new UsuarioModel();
            System.out.println("e = " + e.getMessage());

            return "{\"Accion\":\"error\"}";
        }

    }

    public String upUsuario(UsuarioModel usuario, int id) {
        Conexion con = new Conexion();
        String sql = "UPDATE usuario SET nombre=?,ciudad=?,telefono=?,idTipo=? WHERE id=?";
        try {
            PreparedStatement pstm = con.getCon().prepareStatement(sql);
            pstm.setString(1, usuario.getNombre());
            pstm.setString(2, usuario.getCiudad());
            pstm.setString(3, usuario.getTelefono());
            pstm.setInt(4, usuario.getIdTipo());
            pstm.setInt(5, id);
            return "{\"Accion\":\"" + pstm.executeUpdate() + "\"}";
        } catch (Exception e) {
            UsuarioModel usu = new UsuarioModel();
            System.out.println("e = " + e.getMessage());
            return "{\"Accion\":\"error\"}";
        }

    }
}
