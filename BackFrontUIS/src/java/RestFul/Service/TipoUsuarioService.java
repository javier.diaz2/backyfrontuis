/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFul.Service;

import RestFul.Model.Conexion;
import RestFul.Model.TipoUsuarioModel;
import RestFul.Model.UsuarioModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author SENA
 */
public class TipoUsuarioService {

    public ArrayList<TipoUsuarioModel> getTipoUsuarios() {
        ArrayList<TipoUsuarioModel> lista = new ArrayList<>();
        Conexion con = new Conexion();
        String sql = "SELECT * FROM tipoUsuario";

        try {
            Statement stm = con.getCon().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                TipoUsuarioModel tipoUsuario = new TipoUsuarioModel();
                tipoUsuario.setIdTipo(rs.getInt("idTipo"));
                tipoUsuario.setDescripcion(rs.getString("descripcion"));

                lista.add(tipoUsuario);
            }
        } catch (Exception e) {

            return lista;
        }

        return lista;
    }

    public TipoUsuarioModel getTipoUsuario(int id) {
        TipoUsuarioModel tipoUsuario = new TipoUsuarioModel();
        Conexion con = new Conexion();
        String sql = "SELECT * FROM tipoUsuario WHERE idTipo = ?";

        try {
            PreparedStatement pstm = con.getCon().prepareStatement(sql);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {

                tipoUsuario.setIdTipo(rs.getInt("idTipo"));
                tipoUsuario.setDescripcion(rs.getString("descripcion"));

            }
        } catch (Exception e) {
            System.out.println("e = " + e.getMessage());
            return tipoUsuario;
        }

        return tipoUsuario;
    }
}
