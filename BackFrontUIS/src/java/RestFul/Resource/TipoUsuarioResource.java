/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFul.Resource;

import RestFul.Model.TipoUsuarioModel;
import RestFul.Model.UsuarioModel;
import RestFul.Service.TipoUsuarioService;
import RestFul.Service.UsuarioService;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author SENA
 */
@Path("TipoUsuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TipoUsuarioResource {
    
    TipoUsuarioService servicio = new TipoUsuarioService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<TipoUsuarioModel> getTiposUsuarios() {
        
        return servicio.getTipoUsuarios();
    }
    
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public TipoUsuarioModel getTipoUsuario(@PathParam("id") int id) {
        
        return servicio.getTipoUsuario(id);
    }
    
}
