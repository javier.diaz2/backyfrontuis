/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFul.Resource;

import RestFul.Model.Conexion;
import RestFul.Model.UsuarioModel;
import RestFul.Service.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author SENA
 */
@Path("Usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsuarioResource {

    UsuarioService servicio = new UsuarioService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<UsuarioModel> getUsuarios() {
        
        return servicio.getUsuarios();
    }

    @GET
    @Path("/{id}")
    public UsuarioModel getUsuario(@PathParam("id") int id) {
        UsuarioModel usu =new UsuarioModel();
        
        usu=servicio.getUsuario(id);
        return usu;
    }

    @DELETE
    @Path("/{id}")
    public String delUsuario(@PathParam("id") int id) {

        return servicio.delUsuario(id);
    }

    @POST
    public String setUsuario(String JSON) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        UsuarioModel usuario = gson.fromJson(JSON, UsuarioModel.class);
        return servicio.setUsuario(usuario);
    }

    @PUT
    @Path("/{id}")
    public String upUsuario(@PathParam("id") int id,String JSON) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        UsuarioModel usuario = gson.fromJson(JSON, UsuarioModel.class);
        return servicio.upUsuario(usuario,id);
    }

}
