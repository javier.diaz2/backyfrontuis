package ejemploproyecto;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    private Connection con;
    private int servidor=2;
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String HOST = "localhost:3306";
    private String DB = "c3s56formador";
    private String URL = "jdbc:mysql://" + HOST + "/" + DB + "?serverTimezone=UTC";
    private String USERNAME = "c3s56formador";
    private String PASSWORD = "sn7mGUMR";

    public Conexion() {
        if (servidor == 1) {

        } else if (servidor == 2) {
            DB = "almacen";
            URL = "jdbc:mysql://" + HOST + "/" + DB + "?serverTimezone=UTC";
            USERNAME = "root";
            PASSWORD = "";
        }

        try {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            con.setTransactionIsolation(8);
            System.out.println("Conexión exitosa");
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public String conectar() {
        try {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            con.setTransactionIsolation(8);
            return "\"Accion\": \"bien\"";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public Connection getCon() {
        return con;
    }

    public void desconectar() {
        try {
            if (con != null) {
                con.close();
                System.out.println("La desconexion fue exitosa");
            }

        } catch (Exception excepcion) {
            System.out.println("Ha ocurrido un error al desconectar  " + excepcion.getMessage());
        }
    }

}
